extends Object
class_name TransformScriptIconsConfig

static var BGColor = load("res://Scripts/TransformOperations/TransformScriptModes/Icons/IconMaster_BGColor.png")
static var BGTexture = load("res://Scripts/TransformOperations/TransformScriptModes/Icons/IconMaster_BGTexture.png")
static var Tear = load("res://Scripts/TransformOperations/TransformScriptModes/Icons/IconMaster_Tear.png")
static var TearExpand = load("res://Scripts/TransformOperations/TransformScriptModes/Icons/IconMaster_TearExpand.png")
