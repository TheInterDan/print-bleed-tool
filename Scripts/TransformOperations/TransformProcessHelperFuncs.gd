extends Object
class_name TransformProcessHelperFuncs

static func get_transform_scripts() -> Array[Script]: 
	#Assuming the "TransformOperations" folder structure won't be modified.
	var self_path = TransformProcessHelperFuncs.new().get_script().resource_path
	var dir = DirAccess.open(self_path + "/../TransformScriptModes")
	var scripts : Array[Script] = []
	for file in dir.get_files():
		var new_script = load(dir.get_current_dir() + "/" + file) as Script
		if new_script: scripts.append(new_script)
	return scripts
