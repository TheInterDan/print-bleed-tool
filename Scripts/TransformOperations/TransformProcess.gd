extends Node
class_name TransformProcess

func create_tree():
	var viewport := SubViewport.new()
	var sprite := Sprite2D.new()
	sprite.name = "ESPRAIT"
	viewport.add_child(sprite, true)
	sprite.print_tree()
	print(sprite.get_parent())
	viewport.size = Vector2i(256, 256)
	
	sprite.position = Vector2i.ONE * 128
	return viewport.get_texture()

func open_image(path_relative_to_res = "."):
	var dir = DirAccess.open("res://")
	var icon_res = load(dir.get_current_dir() + "icon.svg")
	var icon := load(dir.get_current_dir() + "icon.svg") as CompressedTexture2D
	print(icon.get_image().get_format())
	
	var img = Image.load_from_file(dir.get_current_dir() + "icon.svg")
	print(img.get_format())
	print(img.get_height())
	var bg = Image.create(520,780,0, img.get_format())
	bg.fill(Color.WHITE)
	
	img.resize(img.get_width() * 2, img.get_height() * 2, Image.INTERPOLATE_CUBIC)
	img.save_png(dir.get_current_dir() + "icon.png")
	
	bg.blend_rect(img, Rect2(Vector2.ZERO, img.get_size()), Vector2i(20,20))
	bg.save_png(dir.get_current_dir() + "OA.png")
