extends PanelContainer

@export var transform_info = TransformInfo.new()

@onready var scan_path = DirAccess.open("res://").get_current_dir(false)
# Called when the node enters the scene tree for the first time.
func _ready():
	$%ScanPathButton.text = scan_path
	$%ScanPathButton.pressed.connect(select_scan_file)
	
	$%ExportButton.pressed.connect(_on_export)
	
	populate_bleed_style_dropdown()

func populate_bleed_style_dropdown():
	var transform_scripts = TransformProcessHelperFuncs.get_transform_scripts()
	for transform_script in transform_scripts:
		var transform_object = transform_script.new()
		%BleedStyle.add_icon_item(transform_object.get_icon(), transform_object.get_style_name())

func select_scan_file():
	%FileDialog.file_mode = FileDialog.FILE_MODE_OPEN_DIR
	%FileDialog.current_dir = scan_path
	%FileDialog.popup_centered(Vector2i(800,600))
	get_tree().root.add_child(FileDialog.new())
	%FileDialog.dir_selected.connect(_on_scan_file_selected, CONNECT_ONE_SHOT)

func _on_scan_file_selected(dir):
	scan_path = dir
	$%ScanPathButton.text = scan_path
	
	scan_for_first_image()

func scan_for_first_image():
	var dir = DirAccess.open(scan_path)
	
	for file in dir.get_files():
		print(file)
		var extension = file.get_extension().to_lower()
		print(extension)
		if extension in ["png", "jpg", "jpeg"]:
			print(file)
			var img = Image.load_from_file(scan_path + "/" + file)
			if img:
				set_content_size(img.get_size())
				break

func set_content_size(new_size : Vector2):
	$%ContentRes.set_values_by_vector(new_size)
	
	var pad = %PxPad.get_values()
	pad.x += pad.z
	pad.y += pad.w
	
	var new_total_size = new_size + Vector2(pad.x, pad.y)
	$%TotalRes.set_values_by_vector(new_total_size)
	
	var pad_percent = ((new_total_size / new_size) - Vector2.ONE) * 100
	$%PercentPad.set_value(pad_percent.x, 0)

func set_total_size(new_total_size : Vector2):
	$%TotalRes.set_values_by_vector(new_total_size)

func _on_export():
	var dir = DirAccess.open(scan_path)
	var export_dir = DirAccess.make_dir_recursive_absolute(dir.get_current_dir() + "/export")
	
	
