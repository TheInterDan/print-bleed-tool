@tool
extends HBoxContainer

@onready var label = $Label
@onready var spin_box = $SpinBox

@export var label_text = "Label" : set = set_label_text
@export var amount : float = 0 : set = set_value
@export var suffix = "" : set = set_suffix

#Set values to UI elements from Main.
func set_label_text(value):
	label_text = value
	if label: label.text = label_text
func set_value(value):
	amount = value
	if spin_box: spin_box.value = amount
func set_suffix(value):
	suffix = value
	if spin_box: spin_box.suffix = suffix

#Set values to Main from UI elements.
signal value_changed(value : float)

func _ready():
	label.text = label_text
	spin_box.value = amount
	$SpinBox.value_changed.connect(_on_value_changed)

func _on_value_changed(value):
	value_changed.emit(value)
