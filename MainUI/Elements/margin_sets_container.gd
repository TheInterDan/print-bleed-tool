@tool
extends "labeled_container.gd"

#This class emits a Vector4 whenever any value inside it is changed
#by the set_value function. It will automatically adjust the "All" field
#so it's active if all the other fields are equal.

signal value_changed(value : Vector4)
var lock_signal = 0

@onready var all = $%All
@onready var left = $%Left
@onready var top = $%Top
@onready var right = $%Right
@onready var bottom = $%Bottom
@onready var components_list = [all, left, top, right, bottom]

@export var suffix = "" : set = set_suffix_text
func set_suffix_text(value):
	suffix = value
	if components_list: update_suffixes()
func update_suffixes():
	for component in components_list:
		if component: component.suffix = suffix

func _ready():
	super._ready()
	update_suffixes()
	connect_components()

func connect_components():
	var array_size = components_list.size()
	for i in array_size:
		var idx = array_size - 1 - i
		components_list[idx].value_changed.connect(set_value.bind(idx))

func get_values() -> Vector4:
	var vector = Vector4.ZERO
	vector.x = left.amount
	vector.y = top.amount
	vector.z = right.amount
	vector.w = bottom.amount
	return vector

#0 = all, 1 = left, 2 = top, 3 = right, 4 = bottom
func set_value(value : float, component_idx : int):
	if lock_signal: return
	lock_signal += 1
	
	var component = components_list[component_idx]
	component.amount = value
	
	if component_idx == 0:
		set_values_by_vector.bind(Vector4.ONE * value).call_deferred()
	else:
		_update_the_all_component()
	
	lock_signal -= 1
	emit_value_changed_signal()

func set_values_by_vector(new_values : Vector4):
	lock_signal += 1
	var vector_components = ["x", "y", "z", "w"]
	for i in range(1,5):
		var value = new_values[vector_components[i-1]]
		var component = components_list[i]
		component.amount = value
	
	_update_the_all_component()
	lock_signal -= 1

var all_same = true
func _update_the_all_component():
	all_same = left.amount == top.amount and left.amount == right.amount \
			and left.amount == bottom.amount
	
	if all_same:
		all.amount = left.amount
	else:
		all.amount = 0

func emit_value_changed_signal():
	if lock_signal: return
	value_changed.emit(get_values)
