@tool
extends PanelContainer

@onready var label = $%Label
@export var label_text = "Label" : set = set_label_text
func set_label_text(value):
	label_text = value
	if label: label.text = label_text
func _ready():
	label.text = label_text
