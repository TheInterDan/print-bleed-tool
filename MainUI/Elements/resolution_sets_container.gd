@tool
extends "labeled_container.gd"

#This class emits a Vector2 whenever any value inside it is changed
#by the set_value function. If keep_aspect is true, it will also change
#the other value.

signal value_changed(value: Vector2)
var lock_signal = 0

@onready var width = $%Width
@onready var height = $%Height
@onready var components_list = [width, height]

@export var suffix = "" : set = set_suffix_text
func set_suffix_text(value):
	suffix = value
	if components_list: update_suffixes()

@export var keep_aspect: bool = false
var prev_aspect = Vector2.ONE : set = set_prev_aspect
func set_prev_aspect(value):
	prev_aspect = value
	clamp_aspect()
func clamp_aspect():
	if prev_aspect.x == 0: prev_aspect.x = 1
	if prev_aspect.y == 0: prev_aspect.y = 1

func update_suffixes():
	for component in components_list:
		if component:
			component.suffix = suffix

func _ready():
	super._ready()
	update_suffixes()
	connect_components()

func connect_components():
	for i in range(components_list.size()):
		components_list[i].value_changed.connect(set_value.bind(i))

func get_values() -> Vector2:
	return Vector2(width.amount, height.amount)

func set_value(value: float, component_idx: int):
	if lock_signal: return
	lock_signal += 1
	
	var component = components_list[component_idx]
	component.amount = value
	
	if keep_aspect:
		if component_idx == 0:  # Width component changed
			height.amount = value * (prev_aspect.x / prev_aspect.y)
		else:  # Height component changed
			width.amount = value * (prev_aspect.y / prev_aspect.x)
	
	lock_signal -= 1
	emit_value_changed_signal()

func set_values_by_vector(new_values: Vector2):
	lock_signal += 1
	var vector_components = ["x", "y"]
	for i in range(components_list.size()):
		var value = new_values[vector_components[i]]
		var component = components_list[i]
		component.amount = value
	
	prev_aspect = new_values
	
	emit_value_changed_signal()
	lock_signal -= 1

func emit_value_changed_signal():
	if lock_signal:
		return
	value_changed.emit(get_values())
